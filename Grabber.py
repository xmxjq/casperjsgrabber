'''
Created on 2013-3-4

@author: xmxjq
'''

from Beans.BaseModel import BaseModel
from SubGrabberPool import SubGrabberPool
from UrlParser import UrlParser
from asyncthreads.reactor import Reactor
from sqlalchemy import create_engine
from threadpool import makeRequests
import ConfigParser
import threadpool
import timeit
    
class Grabber(object):
    # Main Class for the Grabber
    
    def __init__(self, url_file_path = None):
        self.__init_config()
        self.engine = create_engine(self.db_path,
                                    pool_size = self.pool_size,
                                    encoding = 'utf8',
                                    convert_unicode = True,
                                    echo = self.sql_debug_mode)
        self.init_db()
        if url_file_path is not None:
        # url_file_path is none means that the grabber is running in web interact mode.
            self.parser = UrlParser(url_file_path)
        else:
            self.parser = UrlParser()
        self.pool = threadpool.ThreadPool(self.pool_size)
        self.reactor = Reactor(self.pool_size)
        self.grabber_pool = SubGrabberPool(self.pool_size, self.engine)
        
    def __init_config(self):
        cf = ConfigParser.ConfigParser()
        cf.read("config.ini")
        self.db_path = cf.get("Database", "DB_PATH")
        self.phantomjs_path = cf.get("PhantomJS", "PHANTOMJS_PATH")
        self.casperjs_path = cf.get("CasperJS", "CASPER_PATH")
        self.pool_size = int(cf.get("ThreadPool", "POOL_SIZE"))
        self.debug_mode = cf.get("Debug","DEBUG_MODE")
        sql_debug_mode = cf.get("Debug","SQL_DEBUG_MODE")
        if sql_debug_mode == "1":
            self.sql_debug_mode = True
        else:
            self.sql_debug_mode = False
        
        
    def init_db(self):
        BaseModel.metadata.create_all(self.engine)

    def drop_db(self):
        BaseModel.metadata.drop_all(self.engine)
        
    def do_grab(self):
        self.parser.parse_url_file()
#        requests = makeRequests(self.grab_request, self.parser.url_list)
#        [self.pool.putRequest(req) for req in requests]
#        self.pool.wait()
#        self.pool.dismissWorkers(self.pool_size, do_join=True)
        self.reactor.start()
        [self.reactor.call_in_thread(self.grab_request, (url,)) for url in self.parser.url_list]
        self.reactor.shutdown()
        self.grabber_pool.destory_pool()
        
    def web_grab_request(self, url):
        if not self.reactor.isAlive():
            self.reactor.start()
        target_url = self.parser.initialize_baseurl(url)
        self.reactor.call_in_thread(self.grab_request, (target_url,))
        
    def web_grab_request_and_wait_for_result(self, url):
        if not self.reactor.isAlive():
            self.reactor.start()
        target_url = self.parser.initialize_baseurl(url)
        result = self.reactor.call_in_thread(self.grab_request, (target_url,))
        while not result.has_result():
            pass
    
    def web_grab_end(self):
        self.reactor.join()
        self.reactor.shutdown()
        self.grabber_pool.destory_pool()
        
    def grab_request(self, url):
        subgrabber = self.grabber_pool.get_available_subgrabber()
        while subgrabber is None:
            subgrabber = self.grabber_pool.get_available_subgrabber()
        try:
            subgrabber.grab_root(url)
        finally:
            result = self.grabber_pool.release_subgrabber(subgrabber)
            while not result:
                result = self.grabber_pool.release_subgrabber(subgrabber)
        
        
if __name__ == '__main__':
    s = """\
    from Grabber import Grabber
    grabber = Grabber(sys.argv[1])
    grabber.do_grab()
    pass
    """
    print timeit.timeit(stmt=s, number=1)
    pass
    

