#-*- coding:utf-8 -*-
from Grabber import Grabber
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker
from urls_config import urls
from web import form
from web.contrib.template import render_jinja #引入web.py对调用jinja2的模块
import web

app = web.application(urls, globals())

render = render_jinja(
    'templates',#模板存放的目录名称
    encoding = 'utf-8',#模板使用的编码
)

test_form = form.Form(
            form.Textbox('url'),
        )

grabber = Grabber()

def load_sqla(handler):
    web.ctx.orm = scoped_session(sessionmaker(autocommit=False,
                                 autoflush=False,
                                 bind=grabber.engine))
    try:
        return handler()
    except web.HTTPError:
        web.ctx.orm.rollback()
        raise
    except:
        web.ctx.orm.rollback()
        raise
    finally:
        web.ctx.orm.rollback()

app.add_processor(load_sqla)

class index:
    def GET(self):        
        f = test_form()
        return render.main(f=f)
#    
#    def POST(self): 
#        f = test_form() 
#        if not f.validates(): 
#            return render.main(f=f)
#        else:
#            # form.d.boe and form['boe'].value are equivalent ways of
#            # extracting the validated arguments from the form.
#            grabber.web_grab_request(str(f['url'].value))
#            return f['url'].value

if __name__ == "__main__":
#    web.wsgi.runwsgi = lambda func, addr=None: web.wsgi.runfcgi(func, addr)
    app.run()