'''
Created on 2013-3-25

@author: xmxjq
'''
import urlparse

class UrlParser(object):
    '''
    classdocs
    This Class is designed to parse the url list file and control the Grabber's work.
    '''


    def __init__(self, url_file_path=None):
        '''
        Constructor
        '''
        self.url_file_path = url_file_path
        self.url_list = []
        
    def parse_url_file(self, new_url_file_path=None):
        # if new url file is given, change to new url file
        if new_url_file_path is not None:
            self.url_file_path = new_url_file_path
        self.url_list = []
        filehandle = open(self.url_file_path, 'r')
        raw_url_list = filehandle.readlines()
        filehandle.close()
        
        for url in raw_url_list:
            url = url.strip('\n')
            self.url_list.append(self.initialize_baseurl(url))
            
    def initialize_baseurl(self, url):         
        # fix up URL without scheme HTTP to URL with HTTP
        if not isinstance(url, str):
            return 'blank:'
        scheme, netloc, path, query, fragment = urlparse.urlsplit(url)

        # Convert netloc to lowercase. This is what a normal browser will do.
        # Some obfuscation may use this feature
        if scheme == "http":
            netloc = netloc.lower()
            url = urlparse.urlunsplit((scheme,netloc,path,query,fragment))
        if len(scheme) == 0:
            url = '%s://%s'   % ("http", url.lower())
        return url