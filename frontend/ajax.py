'''
Created on 2013-9-4

@author: xmxjq
'''
from main import grabber
import json
import web

class SubmitUrl(object):
    def GET(self):        
        data = dict(result='0') 
        web.header('Content-Type', 'application/json') 
        return json.dumps(data)
    
    def POST(self):
        user_data = web.input()
        grabber.web_grab_request_and_wait_for_result(str(user_data.url))
        data = dict(result='1')
        web.header('Content-Type', 'application/json') 
        return json.dumps(data)