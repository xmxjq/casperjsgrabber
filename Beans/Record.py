'''
Created on 2013-3-26

@author: xmxjq
'''
from Beans.BaseModel import BaseModel
from sqlalchemy import Column
from sqlalchemy.sql.expression import func
from sqlalchemy.types import VARCHAR, Integer, TEXT, DateTime

class Record(BaseModel):
    __tablename__ = 'record'
    __table_args__ = {'mysql_charset':'utf8'}

    id = Column(Integer, primary_key=True)
    baseurl = Column(TEXT)
    baseurl_hash = Column(VARCHAR(32), index=True)
    root_node_id = Column(Integer)
    virus_scan_returncode = Column(Integer)
    virus_scan_result = Column(VARCHAR(4096))
    modify_time = Column(DateTime, default=func.now())
    
    def __init__(self, baseurl, baseurl_hash, root_node_id=None, virus_scan_returncode=-1, virus_scan_result=None):
        self.baseurl = baseurl
        self.baseurl_hash = baseurl_hash
        self.root_node_id = root_node_id
        self.virus_scan_returncode = virus_scan_returncode
        self.virus_scan_result = virus_scan_result
        
    def __repr__(self):
        return "<Record('%s', '%s', '%s', '%s', %s')>" % (self.id, self.baseurl, self.baseurl_hash, self.root_node_id, self.virus_scan_result)