'''
Created on 2013-3-25

@author: xmxjq
'''

from Beans.BaseModel import BaseModel
from sqlalchemy import Column
from sqlalchemy.dialects.mysql.base import LONGBLOB
from sqlalchemy.schema import ForeignKey
from sqlalchemy.sql.expression import func
from sqlalchemy.types import Integer, VARCHAR, TEXT, DateTime
import zlib

class Node(BaseModel):
    __tablename__ = 'node'
    __table_args__ = {'mysql_charset':'utf8'}

    id = Column(Integer, primary_key=True)
    url = Column(TEXT)
    node_type = Column(VARCHAR(40))
    html = Column(LONGBLOB)
    parent_node_id = Column(Integer)
    root_node_id = Column(Integer)
    record_id = Column(Integer, ForeignKey('record.id', onupdate="cascade", ondelete="cascade"))
    modify_time = Column(DateTime, default=func.now())
    
    def __init__(self, url, node_type, html, record_id, parent_node_id=None, root_node_id=None):
        self.url = url
        self.node_type = node_type
        if html is not None:
            self.html = zlib.compress(html.encode('utf8'))
        else:
            self.html = zlib.compress("Error while creating Node!")
        self.parent_node_id = parent_node_id
        self.root_node_id = root_node_id
        self.record_id = record_id
            
    def __repr__(self):
        return "<Node('%s','%s','%s','%s','%s','%s','%s')>" % (self.id, self.url, self.node_type, self.html, self.parent_node_id, self.root_node_id, self.record_id)