var casper = require('casper').create({
	stepTimeout: 30000,
	onStepTimeout: function(){		
				output['url'] = casper.cli.get(0);
				output['iframe_list'] = [];
				output['script_list'] = [];
                output['raw_html'] = "Timeout!";
                this.echo(JSON.stringify(output));
                this.exit();
            }
});
var utils = require('utils');
var output = new Object();

var type = casper.cli.get('type'),
    target = casper.cli.get(0);

if( type == 'all') {
	casper.start(target, function() {
	    var iframe_list = this.evaluate(function () {
	    	var iframes = document.querySelectorAll('iframe');
	    	return [].map.call(iframes, function(element) {
		        return element.src;
			});
		});
		var script_list = this.evaluate(function () {
	    	var scripts = document.querySelectorAll('script');
	    	return [].map.call(scripts, function(element) {
		        return element.src;
			});
		});
		output['url'] = target;
		output['iframe_list'] = iframe_list;
		output['script_list'] = script_list;
		output['raw_html'] = this.getHTML();	
		this.echo(JSON.stringify(output));
	});
}
else if( type == 'html') {
	casper.start(target, function() {
		output['url'] = target;
		output['raw_html'] = this.getHTML();	
		this.echo(JSON.stringify(output));
	});
}
else {
	casper.start('blank:', function() {
		var string = [
		'Test.js Usage:\n',
		'test.js --type=[type] [target]\n',
		'[type] should be the following:\n',
		'	all html\n',
		'[target] should be the url\n'
		].join('');
	    this.echo(string);
	});
}

casper.run();