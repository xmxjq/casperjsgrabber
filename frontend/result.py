'''
Created on 2013-7-8

@author: xmxjq
'''
from Beans.Node import Node
from Beans.Record import Record
from main import render
from sqlalchemy.sql.expression import func
import web
import zlib

class ResultList(object):
    def GET(self):
        query = web.ctx.orm.query(Record).order_by(Record.modify_time.desc()).limit(50)
        records=query.all()
        for record in records:
            record.node_number = web.ctx.orm.query(func.count('*')).filter(Node.root_node_id == record.root_node_id).scalar() + 1
        return render.resultlist(records=records)
    
class SingleResult(object):
    def GET(self, record_id):
        record_query = web.ctx.orm.query(Record).filter(Record.id == int(record_id))
        record = record_query.first()
        if record is None:
            return web.notfound()
        root_node_query = web.ctx.orm.query(Node).filter(Node.id == record.root_node_id)
        root_node = root_node_query.first()
        root_node.de_html = zlib.decompress(root_node.html)
        sub_node_list = web.ctx.orm.query(Node).filter(Node.root_node_id == record.root_node_id).all()
        for node in sub_node_list:
            node.de_html = zlib.decompress(node.html)
        return render.singleresult(record=record, root_node=root_node, sub_node_list=sub_node_list)