'''
Created on 2013-4-2

@author: xmxjq
'''
from Beans.Node import Node
from Beans.Record import Record
import ConfigParser
import chardet
import hashlib
import json
import os
import subprocess
import tempfile
import time
import traceback
import urllib
import urllib2
import urlparse

class SubGrabber(object):
    
    def __init__(self, dbsession, name):
        self.__init_config()
        self.name = name
        self.db_session = dbsession
        self.current_record_id = None
        self.iframe_list_cache = []
        self.script_list_cache = []
        self.url_hash_map = {}
        self.errfile = open(self.name + "casperjserror.log",'a+')
        
    def __del__(self):
        self.errfile.flush()
        os.fsync(self.errfile)
        self.errfile.close()

    def __init_config(self):
        cf = ConfigParser.ConfigParser()
        cf.read("config.ini")
        self.phantomjs_path = cf.get("PhantomJS", "PHANTOMJS_PATH")
        self.casperjs_path = cf.get("CasperJS", "CASPER_PATH")
        self.debug_mode = cf.get("Debug","DEBUG_MODE")
        self.system_type = cf.get("SystemType","TYPE")
        if self.system_type == 'windows':
            self.args = "casperjs.bat " + self.phantomjs_path + '\\phantomjs.exe ' + \
                '"' + self.casperjs_path + '\\bin\\bootstrap.js" ' + \
                '--casper-path="' + self.casperjs_path + '" ' + \
                '--cli ' + 'test.js ' +'--type=all '
            self.args_html = "casperjs.bat " + self.phantomjs_path + '/phantomjs ' + \
                '"' + self.casperjs_path + '/bin/bootstrap.js" ' + \
                '--casper-path="' + self.casperjs_path + '" ' + \
                '--cli ' + 'test.js ' +'--type=html '
        else:
            self.args = self.phantomjs_path + '/phantomjs ' + \
                '"' + self.casperjs_path + '/bin/bootstrap.js" ' + \
                '--casper-path="' + self.casperjs_path + '" ' + \
                '--cli ' + 'test.js ' +'--type=all '
            self.args_html = self.phantomjs_path + '/phantomjs ' + \
                '"' + self.casperjs_path + '/bin/bootstrap.js" ' + \
                '--casper-path="' + self.casperjs_path + '" ' + \
                '--cli ' + 'test.js ' +'--type=html '
        

    def grab_root(self, root_url):
        print self.name + ' Grabbing: ' + root_url
        
        self.db_session()
        
        self.current_record_id = None
        self.iframe_list_cache = []
        self.script_list_cache = []
        self.url_hash_map = {}        
        
        print self.name + ' [root] ' + root_url
        
        root_url_hash = hashlib.md5(root_url.encode("utf8")).hexdigest()
        query = self.db_session.query(Record)
        if query.filter(Record.baseurl_hash == self.to_unicode(root_url_hash)).first() is not None:
            if self.debug_mode == "1":
                print self.name + " Record hash: " + root_url_hash
            print self.name + " Record already exists: " + root_url
            print self.name + ' Grab Finish: ' + root_url
            return
        
        rootdata = self.grab_single(root_url)
        rootdata['url'] = self.to_unicode(rootdata['url']);
        rootdata['raw_html'] = self.to_unicode(rootdata['raw_html']);
        rootdata['url_hash'] = hashlib.md5(rootdata['url'].encode("utf8")).hexdigest()
        
        new_record = Record(self.to_unicode(rootdata['url']), rootdata['url_hash'])
        self.db_session.add(new_record)
        self.db_session.commit()
        self.current_record_id = new_record.id
        root_node = Node(rootdata['url'], u'root', rootdata['raw_html'], self.current_record_id)
        self.db_session.add(root_node)
        self.db_session.flush()
        new_record.root_node_id = root_node.id
        self.db_session.merge(new_record)
        self.db_session.commit()
        
        self.add_url_to_cache(rootdata['url'], root_node.id, root_node.id, rootdata)
        
        while len(self.iframe_list_cache) or len(self.script_list_cache):
            while len(self.iframe_list_cache):                
                iframe = self.iframe_list_cache.pop()
                print self.name + ' [iframe] ' + iframe[1]
                resultdata = self.grab_single(iframe[1])
                resultdata['url'] = self.to_unicode(resultdata['url']);
                resultdata['raw_html'] = self.to_unicode(resultdata['raw_html']);
                node = Node(resultdata['url'], u'iframe', resultdata['raw_html'], self.current_record_id, iframe[0], root_node.id)
                self.db_session.add(node)
                self.db_session.commit()
                self.add_url_to_cache(resultdata['url'], node.id, root_node.id, resultdata)
                
            while len(self.script_list_cache):                
                script = self.script_list_cache.pop()
                print self.name + ' [script] ' + script[1]
                resultdata = self.grab_single_html_only(script[1])
                resultdata['url'] = self.to_unicode(resultdata['url']);
                resultdata['raw_html'] = self.to_unicode(resultdata['raw_html']);
                node = Node(resultdata['url'], u'script', resultdata['raw_html'], self.current_record_id, script[0], root_node.id)
                self.db_session.add(node)
                self.db_session.commit()
                
        
        self.db_session.remove()
        print self.name + ' Grab Finish: ' + root_url
                
    def grab_single(self, url):
        out_buffer = tempfile.TemporaryFile()
        if self.system_type == 'windows':
            p = subprocess.Popen( self.args + url, 
                                  stdout = out_buffer, 
                                  stderr = self.errfile,
                                )
        else:
            p = subprocess.Popen( self.args + url, 
                                  stdout = out_buffer, 
                                  stderr = self.errfile,
                                  shell = True,
                                  close_fds = True,
                                  executable="/bin/bash"
                                )
        for i in range(0,100):
            if p.poll() is not None:
                break
            else:                
                time.sleep(1)
                if i == 99:
                    if self.debug_mode == "1":
                        print self.name + " Killing CasperJS"
                    p.terminate()
                    out_buffer.close()
                    resultdata = {}
                    resultdata['url'] = url
                    resultdata['iframe_list'] = [];
                    resultdata['script_list'] = [];
                    resultdata['raw_html'] = u"CasperJS Run Timeout!"
                    return resultdata;
                
        out_buffer.seek(0)        
        resultjson = out_buffer.read()
        if self.debug_mode == "1":
            print resultjson
        try:
            resultdata = json.loads(resultjson)
        except Exception,ex:
            if self.debug_mode == "1":
                print Exception,":",ex
            out_buffer.close()
            resultdata = {}
            resultdata['url'] = url
            resultdata['iframe_list'] = [];
            resultdata['script_list'] = [];
            resultdata['raw_html'] = u"CasperJS Error!"
        
        out_buffer.close()
        return resultdata;
        
    def grab_single_html_only(self, url):
        try:
            content = urllib2.urlopen(url, timeout=30).read()
        except urllib2.HTTPError:
            content = None
        except Exception:
            content = traceback.format_exc()
        resultdata = {}
        resultdata['url'] = url
        resultdata['raw_html'] = content
        if self.debug_mode == "1":
            print resultdata
        return resultdata;
    
    def add_url_to_cache(self, parent_url, parent_id, root_id, resultdata):
        if resultdata['iframe_list'] is not None:
            for i in resultdata['iframe_list']:
                if i:
                    fixresult = self.fix_url(parent_url, i)
                    if fixresult == u"Unknown scheme":
                        i = urllib.unquote(i)
                        node = Node(u"Raw iframe", u'iframe', self.to_unicode(i), self.current_record_id, parent_id, root_id)
                        self.db_session.add(node)
                        self.db_session.commit()
                    else:
                        url_hash = hashlib.md5(fixresult.encode("utf8")).hexdigest()
                        if not self.url_hash_map.has_key(url_hash):
                            self.url_hash_map[url_hash] = fixresult;
                            self.iframe_list_cache.append((parent_id, fixresult));
        if resultdata['script_list'] is not None:
            for s in resultdata['script_list']:
                if s:
                    fixresult = self.fix_url(parent_url, s)
                    url_hash = hashlib.md5(fixresult.encode("utf8")).hexdigest()
                    if not self.url_hash_map.has_key(url_hash):
                        self.url_hash_map[url_hash] = fixresult;
                    self.script_list_cache.append((parent_id, fixresult));
    
    def fix_url(self, base_url, url):
        base_scheme, base_netloc, base_path, base_query, base_fragment = urlparse.urlsplit(base_url)
         
        # fix up relative URLs to absolute URLs
        if not isinstance( url, basestring ):
            if self.debug_mode == "1":
                print "Warning: URL is not a basestring!"            
            return 'blank:'
        scheme, netloc, path, query, fragment = urlparse.urlsplit(url)

        # Convert netloc to lowercase. This is what a normal browser will do.
        # Some obfuscation may use this feature
        if scheme == "http":
            netloc = netloc.lower()
            url = urlparse.urlunsplit((scheme,netloc,path,query,fragment))
        if len(scheme) == 0:
            if url.startswith('/'): 
                url = '%s://%s%s'   % (base_scheme, base_netloc, url)
            elif url.startswith('?'): 
                url = '%s://%s%s%s' % (base_scheme, base_netloc, base_path, url)
            elif url.startswith('#'):
                url = '%s://%s%s%s' % (base_scheme, base_netloc, base_path, url)
            else:
                _url = "%s://%s"    % (base_scheme, base_netloc, )
                if base_path:
                    base_path_split = base_path.strip().split('/')[:-1]
                else:
                    base_path_split = []
                for directory in url.split('/'):
                    if directory == '.':
                        continue
                    elif directory == '..':
                        if len(base_path_split) > 1:
                            base_path_split = base_path_split[:-1]
                    else:
                        base_path_split.append(directory)
                url = _url + '/'.join(base_path_split)
        elif scheme != "http" and scheme != "https":
            if self.debug_mode == "1":
                print "Warning! Unknown scheme:" + scheme
            return u"Unknown scheme"
        return self.to_unicode(url);
    
    def to_unicode(self, s):
        if not isinstance( s, basestring ):
            return s
        if isinstance( s, unicode ):
            return s
        else:
            enc = chardet.detect(s)
            if self.debug_mode == "1":
                print enc
            if enc['encoding'] is not None:
                try:
                    return unicode(s, enc['encoding'])
                except UnicodeDecodeError:
                    try:
                        return unicode(s, 'utf8')
                    except UnicodeDecodeError:
                        return u"Unknown Encoding"
            else:
                try:
                    return unicode(s, 'utf8')
                except UnicodeDecodeError:
                    return u"Unknown Encoding"