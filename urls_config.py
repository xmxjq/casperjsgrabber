'''
Created on 2013-7-8

@author: xmxjq
'''
urls = (
    "/", "index",
    "/ajax/submit_url", "frontend.ajax.SubmitUrl",
    "/result", "frontend.result.ResultList",
    "/result/record/(.+)", "frontend.result.SingleResult",
)