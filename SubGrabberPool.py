'''
Created on 2013-4-9

@author: xmxjq
'''
from SubGrabber import SubGrabber
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker

class SubGrabberPool(object):
    '''
    A Simple SubGrabber Pool used for multithreading.
    '''


    def __init__(self, pool_size, dbengine):
        self.pool_size = pool_size
        self.dbengine = dbengine
        self.db_session = scoped_session(sessionmaker(autocommit=False,
                                 autoflush=False,
                                 bind=dbengine))
        self.subgrabber_available = []
        self.pool_busy = False
        self.in_destory = False
        self.__init_pool()
        
    def __init_pool(self):
        for i in range(0, self.pool_size):
            new_subgrabber = SubGrabber(self.db_session, "Thread" + str(i))
            self.subgrabber_available.append(new_subgrabber)
            
    def get_available_subgrabber(self):
        if not self.pool_busy and not self.in_destory:
            self.pool_busy = True
            grabber = self.subgrabber_available.pop()
            self.pool_busy = False
            return grabber
        else:
            return None
        
    def release_subgrabber(self, grabber):
        if not self.pool_busy:
            self.pool_busy = True
            self.subgrabber_available.append(grabber)
            self.pool_busy = False
            return True
        else:
            return False
        
    def destory_pool(self):
        self.in_destory = True
        while len(self.subgrabber_available) != self.pool_size:
            pass
                        