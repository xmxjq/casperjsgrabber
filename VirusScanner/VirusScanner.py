'''
Created on 2013-5-28

@author: xmxjq
'''
from Beans.Node import Node
from Beans.Record import Record
from sqlalchemy.engine import create_engine
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker
import ConfigParser
import subprocess
import zlib

if __name__ == '__main__':
    cf = ConfigParser.ConfigParser()
    cf.read("../config.ini")
    db_path = cf.get("Database", "DB_PATH")
    engine = create_engine(db_path,
                            encoding = 'utf8',
                            convert_unicode = True)
    db_session = scoped_session(sessionmaker(autocommit=False,
                                 autoflush=False,
                                 bind=engine))    
    db_session()
    
    query = db_session.query(Record)
    args = '"C:\\Program Files\\ESET\\ESET Endpoint Antivirus\\ecls.exe" /log-console /base-dir="C:\\Program Files\\ESET\\ESET Endpoint Antivirus" /log-file=D:\\CaptureHPC\\test\\result.log /log-rewrite D:\\CaptureHPC\\test\\test'
    for record in query.all():
        print "Processing " + record.baseurl
        outputfile = open("D:\\CaptureHPC\\test\\test\\output.dat",'w')
        query2 = db_session.query(Node)
        main_node = query2.filter(Node.id == record.root_node_id).first()
        outputfile.write(main_node.url)
        outputfile.write("\n")
        outputfile.write(zlib.decompress(main_node.html));
        for single_node in query2.filter(Node.root_node_id == record.id).all():
            outputfile.write(single_node.url)
            outputfile.write("\n")
            outputfile.write(zlib.decompress(single_node.html));    
        outputfile.close()
        
        p = subprocess.Popen( args,
                              shell = True,
                              close_fds = True )
        p.wait()
        
        print "Result: " + str(p.returncode)
        logfile = open("D:\\CaptureHPC\\test\\result.log",'r')
        record.virus_scan_returncode = p.returncode
        record.virus_scan_result = unicode(logfile.read(), 'gbk')
        logfile.close()
        db_session.merge(record)
        db_session.commit()
    
    db_session.remove()
    pass